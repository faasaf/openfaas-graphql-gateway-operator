## OpenFaas GraphQl Gateway Operator

A simple operator which watches for openfaas function deployments, reads their labels and generates
a GraphQlGateway CRD with which the [GraphQl Gateway Operator](https://www.gitlab.com/faasaf/graphql-gateway-operator) can use to deploy or configure [GraphQl Gateways](https://www.gitlab.com/faasaf/graphql-gateway). 
This code is based on [ts-operator](https://github.com/kainlite/ts-operator)

### Getting Started

- You will need an existing Kubernetes cluster with Openfaas installed. 
- You will need to install the [GraphQl Gateway Operator](https://www.gitlab.com/faasaf/graphql-gateway-operator)

```bash
git clone https://gitlab.com/faasaf/openfaas-graphql-gateway-operator.git
cd openfaas-graphql-gateway-operator/
make VERSION=develop deploy
```

Now go and create a new function (or edit an existing one).
```bash
faas-cli new echo --lang node14
```

Now we can edit the function yml to add graphql-gateway specific annotations and labels to it.

```yaml
version: 1.0
provider:
  name: openfaas
  gateway: http://localhost:31112
functions:
  echo:
    labels:
      gql_function: true 
    annotations:
      gateways: |
        - name: public-api
          namespace: public
      backend: gateway.openfaas.svc.cluster.local
      schema: |
        type Query {
          echo(body: String!): String
        }
    lang: node14
    handler: ./echo
    image: "localhost:32000/echo:latest"

```

Change the `handler.js`:
```javascript
'use strict'

module.exports = async (event, context) => {
  const result = {
    'body': JSON.stringify(event.body),
    'content-type': event.headers["content-type"]
  }

  return context  
    .status(200)
    .headers({
      'Content-Type': event.headers['content-type']
    })
    .succeed(result.body)
}
```

```bash
faas-cli --gateway=http://localhost:31112 up -f ./echo.yml
```


### Cleanup

```bash
faas-cli --gateway=http://localhost:31112 remove -r ./echo.yml
rm ./echo.yml && rm -r ./echo
make VERSION=develop undeploy
```
