import * as k8s from "@kubernetes/client-node";
import compileGraphQl from "./compileGraphQl";
import compileMappings from "./compileMappings";
import readFunctions from "./readFunctions";
import writeCustomResource from "./writeCustomResource";

const OPENFAAS_FUNCTION_NAMESPACE = process.env.OPENFAAS_FUNCTION_NAMESPACE || "faas-fn";
const OPENFAAS_GATEWAY = process.env.OPENFAAS_GATEWAY || "http://localhost:31112";
const CLUSTER_CONTEXT = process.env.CLUSTER_CONTEXT || "inClusterContext";
console.log("Using Context:", CLUSTER_CONTEXT);
console.log("Using Gateway:", OPENFAAS_GATEWAY);

const kc = new k8s.KubeConfig();
kc.loadFromDefault();
kc.setCurrentContext(CLUSTER_CONTEXT);
const client = kc.makeApiClient(k8s.CustomObjectsApi);
const watch = new k8s.Watch(kc);

const updateGraphqlGateways = async () => {
  return readFunctions({
    openfaasUrl: OPENFAAS_GATEWAY,
  }).then((apis) => {
    if (!apis) {
      return apis;
    }

    Object.values(apis).forEach((functionsResponse) => {
      functionsResponse.schema = compileGraphQl({
        functionsResponse,
      });
      functionsResponse.mapping = compileMappings({
        functionsResponse,
      });
      const resourceDefinition = writeCustomResource({
        functionsResponse,
      });

      return client
        .createNamespacedCustomObject(
          resourceDefinition.group,
          resourceDefinition.version,
          resourceDefinition.namespace,
          resourceDefinition.plural,
          resourceDefinition.body,
        )
        .then((r) => {
          console.log("[openfaas-graphql-gateway]: CRD has been created.");
          return r;
        })
        .catch(() => {
          return client
            .patchNamespacedCustomObject(
              resourceDefinition.group,
              resourceDefinition.version,
              resourceDefinition.namespace,
              resourceDefinition.plural,
              resourceDefinition.body.metadata.name,
              resourceDefinition.body,
              undefined,
              undefined,
              undefined,
              {
                headers: {
                  "Content-Type": "application/merge-patch+json",
                }
              }
            )
            .then((r) => {
              console.log("[openfaas-graphql-gateway]: CRD has been replaced.");
              return r;
            });
        })
        .catch((error) => {
          console.log("[openfaas-graphl-gateway]: Received error", error.body);
          return error;
        });
    });
  });
};

async function onEvent(phase: string, apiObj: any) {
  console.log(`[openfaas-graphql-gateway]: Received event in phase ${phase}.`);
  if (phase === "ADDED" || phase === "MODIFIED" || phase === "DELETED") {
    return updateGraphqlGateways();
  }
}

function onDone(err: any) {
  console.log(`[openfaas-graphql-gateway]: Connection closed. ${err}`);
  watchResource();
}

async function watchResource(): Promise<any> {
  console.log("[openfaas-graphql-gateway]: Watching API");
  return watch.watch(`/apis/apps/v1/namespaces/${OPENFAAS_FUNCTION_NAMESPACE}/deployments`, {}, onEvent, onDone);
}

// The watch has begun
async function main() {
  await watchResource();
}

main();
