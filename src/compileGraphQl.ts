import { buildSchema, GraphQLSchema, printSchema } from "graphql";
import { ReadFunctionsResponse } from "./readFunctions";
import { mergeSchemas } from "@graphql-tools/merge";

export default ({ functionsResponse }: { functionsResponse: ReadFunctionsResponse }): string => {
  let graphQl = "";
  const schemas: GraphQLSchema[] = [];

  Object.keys(functionsResponse.functions).forEach((functionName: string) => {
    const func = functionsResponse.functions[functionName];
    schemas.push(buildSchema(func.schema));
  });

  const schema = mergeSchemas({ schemas });
  const printedSchema = printSchema(schema);
  graphQl = printedSchema;

  if (printedSchema.indexOf("Query") === -1) {
    graphQl = `${printedSchema}
    type Query {
      noQueriesAvailable: String
    }`;
  }

  return graphQl;
};
