import { Map, ReadFunctionsResponse } from "./readFunctions";

export default ({ functionsResponse }: { functionsResponse: ReadFunctionsResponse }): Record<string, string | Map> => {
  const mappings: Record<string, string | Map> = {};
  const modemap: Record<string, number> = {};
  let maxCount = 0;
  let maxBackend = "";

  const addBackend = (url: string) => {
    if (!modemap[url]) {
      modemap[url] = 1;
    } else {
      modemap[url]++;
    }
    if (modemap[url] > maxCount) {
      maxBackend = url;
      maxCount = modemap[url];
    }
  };

  Object.keys(functionsResponse.functions).forEach((functionName: string) => {
    const backend = functionsResponse.functions[functionName].backend;
    let url: string = backend as string;
    if (typeof backend === "object") {
      url = backend.url as string;
    }
    addBackend(url);
  });

  Object.keys(functionsResponse.functions)
    .filter((fn) => functionsResponse.functions[fn].backend !== maxBackend)
    .forEach((fn: string) => {
      const func = functionsResponse.functions[fn];
      mappings[fn] = func.backend;
    });

  mappings.default = maxBackend;
  return mappings;
};
