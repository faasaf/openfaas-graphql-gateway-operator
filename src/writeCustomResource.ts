import { dumpYaml } from "@kubernetes/client-node";
import { ReadFunctionsResponse } from "./readFunctions";

export default ({
  functionsResponse,
}: {
  functionsResponse: ReadFunctionsResponse;
}) => {
  return {
    group: "apps.operator.faasaf.io",
    namespace: functionsResponse.gateway.namespace,
    version: "v1alpha1",
    plural: "graphqlgateways",
    body: {
      apiVersion: "apps.operator.faasaf.io/v1alpha1",
      kind: "GraphQlGateway",
      metadata: {
        name: functionsResponse.gateway.name,
        namespace: functionsResponse.gateway.namespace,
      },
      spec: {
        defaultBackend: functionsResponse.mapping?.default,
        map: `
---
${dumpYaml(functionsResponse.mapping)}
        `,
        schema: functionsResponse.schema,
      },
    },
  };
};
