import { loadYaml } from "@kubernetes/client-node";
import "isomorphic-fetch";

export interface ReadFunctionsOptions {
  openfaasUrl: string;
}

export interface FaasGraphQlGateway {
  name: string;
  namespace: string;
}

export interface FaasFunction {
  name: string;
  backend: string | Map;
  schema: string;
  gateways: FaasGraphQlGateway[];
}

export interface Map {
  url: RequestInfo;
  options?: RequestInit;
  path?: string;
}

export interface ReadFunctionsResponse {
  schema?: string;
  mapping?: Record<string, string | Map>;
  gateway: FaasGraphQlGateway;
  functions: Record<string, FaasFunction>;
}
function validURL(str: string): boolean {
  var pattern = new RegExp(
    "^(https?:\\/\\/)?" + // protocol
      "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
      "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
      "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
      "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
      "(\\#[-a-z\\d_]*)?$",
    "i",
  ); // fragment locator
  return !!pattern.test(str);
}

export default async ({ openfaasUrl }: ReadFunctionsOptions): Promise<Record<string, ReadFunctionsResponse> | null> => {
  if (!openfaasUrl) {
    throw Error(`openfaasUrl should not be empty. Ever!`);
  }
  const backendUrl = `${openfaasUrl}/system/functions`;
  console.log("Fetching functions via ", backendUrl);
  return fetch(backendUrl)
    .then((response) => {
      if (!response.ok) {
        return Promise.reject(null);
      }

      return response.json().then((functionsData: any[]) => {
        const fetchedFunctions: Record<string, FaasFunction> = {};
        const functionsByApis: Record<string, ReadFunctionsResponse> = {};

        const addToApi = (name: string, namespace: string, ff: FaasFunction) => {
          if (!functionsByApis[`${name}-${namespace}`]) {
            functionsByApis[`${name}-${namespace}`] = {
              functions: {},
              gateway: {
                name,
                namespace,
              },
            };
          }

          functionsByApis[`${name}-${namespace}`].functions[ff.name] = ff;
        };

        const parseBackend = (raw: string): string | Map => {
          if (validURL(raw)) {
            return raw;
          }

          return loadYaml(raw) as Map;
        };

        functionsData
          .filter((func: any) => !!func.labels?.gql_function)
          .forEach((func: any) => {
            const ff: FaasFunction = {
              name: func.name,
              backend: parseBackend(func.annotations.backend),
              schema: func.annotations.schema,
              gateways: loadYaml(func.annotations.gateways),
            };

            ff.gateways.forEach((gw) => {
              addToApi(gw.name, gw.namespace, ff);
            });

            fetchedFunctions[func.name as string] = ff;
          });
        return functionsByApis;
      });
    })
    .catch((e) => {
      console.error("[openfaas-graphql-gateway-operator]: ERROR", e);
      return null;
    });
};
