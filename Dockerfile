FROM node:alpine

RUN npm install -g nodemon

ADD . /opt/openfaas-graphql-gateway-operator

WORKDIR /opt/openfaas-graphql-gateway-operator

CMD ["/usr/local/bin/yarn", "start"]