import { expect } from "chai";
import readFunctions, { Map } from "../src/readFunctions";
import fetchMock from "fetch-mock";
import { dumpYaml } from "@kubernetes/client-node";

function assertNotNull<T>(v: T | null): T {
  expect(v).to.exist;
  if (!v) throw new Error();

  return v;
}

describe("readFunctions fetches functions from an openfaas rest api and returns a model containing information on the functions found.", () => {
  afterEach(() => fetchMock.restore());
  it("Filters and returns the functions correctly.", async () => {
    const openfaasUrl = "http://localhost:31112";
    fetchMock.mock(`${openfaasUrl}/system/functions`, {
      status: 200,
      body: [
        {
          name: "haveibeenpwned",
          image: "functions/haveibeenpwned:0.13.0",
          namespace: "faas-fn",
          labels: {
            faas_function: "haveibeenpwned",
            gql_function: true,
          },
          annotations: {
            "prometheus.io.scrape": "false",
            backend: `
${dumpYaml({
  url: "gateway.openfaas.svc.cluster.local",
  options: {
    method: "GET",
  },
})}`,
            schema: `
            type FoundResponse {
              found: Int!
            }
            type Query {
              haveibeenpwned(body: String!): FoundResponse
            }
            `,
            gateways: `
---            
${dumpYaml([
  {
    name: "public-api",
    namespace: "public",
  },
])}
            `,
          },
          replicas: 1,
          availableReplicas: 1,
          createdAt: "2021-07-27T12:51:36Z",
        },
      ],
    });

    const functionsResponse = await readFunctions({
      openfaasUrl,
    });

    expect(functionsResponse).to.not.be.null;
    expect(functionsResponse!["public-api-public"]).to.not.be.null;
    expect(functionsResponse!["public-api-public"]).to.not.be.undefined;
    expect(functionsResponse!["public-api-public"].gateway.name).to.be.equal("public-api");
    expect(functionsResponse!["public-api-public"].gateway.namespace).to.be.equal("public");
    expect(functionsResponse!["public-api-public"].functions?.haveibeenpwned).to.not.be.empty;
    expect(functionsResponse!["public-api-public"].functions?.haveibeenpwned.schema).to.not.be.empty;
    
    expect(functionsResponse!["public-api-public"].functions?.haveibeenpwned.backend).to.not.be.empty;
    const backend = functionsResponse!["public-api-public"].functions?.haveibeenpwned.backend as Map;
    expect(backend.url).to.equal("gateway.openfaas.svc.cluster.local");
    expect(backend.options?.method).to.equal("GET");
   

    expect(functionsResponse!["public-api-public"].functions?.haveibeenpwned.gateways).to.not.be.empty;
    const gateway = functionsResponse!["public-api-public"].functions?.haveibeenpwned.gateways[0];
    expect(gateway.name).to.equal("public-api");
    expect(gateway.namespace).to.equal("public");
  });
});
