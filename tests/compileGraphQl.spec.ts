import { ReadFunctionsResponse } from "../src/readFunctions";
import compileGraphQl from "../src/compileGraphQl";
import { buildSchema } from "graphql";
import { expect } from "chai";

describe("compileGraphQl takes function schemata and groups them together into one schema.", () => {
  it("compiles a GraphQl schema from function data.", () => {
    const functionsResponse: ReadFunctionsResponse = {
      gateway: {
        name: "public-api",
        namespace: "public",
      },
      functions: {
        haveibeenpwnd: {
          name: "haveibeenpwnd",
          schema: `
            type FoundResponse {
              found: Int!
            }
            type Query {
              haveibeenpwned(body: String!): FoundResponse
            }
          `,
          backend: "backend",
          gateways: [
            {
              name: "hello",
              namespace: "world",
            },
          ],
        },
        haveibeenpwnd2: {
          name: "haveibeenpwnd2",
          schema: `
            type FoundResponse {
              found: Int!
            }
            type Query {
              haveibeenpwned2(body: String!): FoundResponse
            }
          `,
          backend: "backend",
          gateways: [
            {
              name: "hello",
              namespace: "world",
            },
          ],
        },
        haveibeenpwnd3: {
          name: "haveibeenpwnd3",
          schema: `
            type FoundResponse {
              found: Int!
            }
            type Query {
              haveibeenpwned3(body: String!): FoundResponse
            }
          `,
          backend: "backend",
          gateways: [
            {
              name: "hello",
              namespace: "world",
            },
          ],
        },
        haveibeenpwnd4: {
          name: "haveibeenpwnd4",
          schema: `
            type FoundResponse {
              found: Int!
            }
            type Query {
              haveibeenpwned4(body: String!): FoundResponse
            }
          `,
          backend: "backend2",
          gateways: [
            {
              name: "hello",
              namespace: "world",
            },
          ],
        },
        haveibeenpwnd5: {
          name: "haveibeenpwnd5",
          schema: `
            type FoundResponse {
              found: Int!
            }
            type Query {
              haveibeenpwned5(body: String!): FoundResponse
            }
          `,
          backend: "backend2",
          gateways: [
            {
              name: "hello",
              namespace: "world",
            },
          ],
        },
      },
    };

    const graphQlSchema = compileGraphQl({
      functionsResponse,
    });

    const builtSchema = buildSchema(graphQlSchema);
    const queryConfig: any = builtSchema.getType('Query')?.toConfig()
    expect(queryConfig).to.not.be.null;
    const expectedFields = [
      'haveibeenpwned',
      'haveibeenpwned2',
      'haveibeenpwned3',
      'haveibeenpwned4',
      'haveibeenpwned5',
    ];

    expectedFields.forEach(f => {
      const fields = Object.keys(queryConfig?.fields);
      expect(fields).to.include(f)
    })

  });
});
