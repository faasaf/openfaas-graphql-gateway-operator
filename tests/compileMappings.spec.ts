import { ReadFunctionsResponse } from "../src/readFunctions";
import compileMappings from "../src/compileMappings";
import { expect } from "chai";

describe("compileMappings reads a function response and reads the backend property.", () => {
  it("combines the most used backend as the default backend.", () => {
    const functionsResponse: ReadFunctionsResponse = {
      gateway: {
        name: "public-api",
        namespace: "public",
      },
      functions: {
        haveibeenpwnd: {
          name: "haveibeenpwnd",
          schema: `
            type FoundResponse {
              found: Int!
            }
            type Query {
              haveibeenpwned(body: String!): FoundResponse
            }
          `,
          backend: "backend",
          gateways: [
            {
              name: "hello",
              namespace: "world",
            },
          ],
        },
        haveibeenpwnd2: {
          name: "haveibeenpwnd2",
          schema: `
            type FoundResponse {
              found: Int!
            }
            type Query {
              haveibeenpwned2(body: String!): FoundResponse
            }
          `,
          backend: {
            url: "backend",
            options: {
              method: 'OPTIONS'
            }
          },
          gateways: [
            {
              name: "hello",
              namespace: "world",
            },
          ],
        },
        haveibeenpwnd3: {
          name: "haveibeenpwnd3",
          schema: `
            type FoundResponse {
              found: Int!
            }
            type Query {
              haveibeenpwned3(body: String!): FoundResponse
            }
          `,
          backend: {
            url: "backend",
            options: {
              method: 'GET'
            }
          },
          gateways: [
            {
              name: "hello",
              namespace: "world",
            },
          ],
        },
        haveibeenpwnd4: {
          name: "haveibeenpwnd4",
          schema: `
            type FoundResponse {
              found: Int!
            }
            type Query {
              haveibeenpwned4(body: String!): FoundResponse
            }
          `,
          backend: "backend2",
          gateways: [
            {
              name: "hello",
              namespace: "world",
            },
          ],
        },
        haveibeenpwnd5: {
          name: "haveibeenpwnd5",
          schema: `
            type FoundResponse {
              found: Int!
            }
            type Query {
              haveibeenpwned5(body: String!): FoundResponse
            }
          `,
          backend: "backend2",
          gateways: [
            {
              name: "hello",
              namespace: "world",
            },
          ],
        },
      },
    };

    const mappings = compileMappings({ functionsResponse });

    expect(mappings.default).to.be.equal("backend");
    expect(mappings.haveibeenpwnd4).to.be.equal("backend2");
    expect(mappings.haveibeenpwnd5).to.be.equal("backend2");
  });
});
