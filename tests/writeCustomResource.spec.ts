import { dumpYaml, loadYaml } from "@kubernetes/client-node";
import { expect } from "chai";
import { Map } from "../src/readFunctions";
import writeCustomResource from "../src/writeCustomResource";

describe("writeCustomResource takes a mapping, schema and function data to write a custom resource for graphql-gateway.", () => {
  it("writes a custom resource object from input data.", () => {
    const specSchema = `
    type FoundResponse {
      found Int!
    }
    type Query {
      haveibeenpwnd(body: String!): FoundResponse
    }
    `;

    const mapping = {
      expected: {
        url: "gateway.openfaas.svc.cluster.local",
        options: {
          method: "GET",
        },
      },
    };

    const expectedResource = {
      group: "apps.operator.faasaf.io",
      namespace: "public",
      version: "v1alpha1",
      plural: "graphqlgateways",
      body: {
        apiVersion: "apps.operator.faasaf.io/v1alpha1",
        kind: "GraphQlGateway",
        metadata: {
          name: "public-api",
          namepspace: "public",
        },
        spec: {
          defaultBackend: mapping.expected.url,
          map: dumpYaml(mapping),
          schema: specSchema,
        },
      },
    };

    const actualResource = writeCustomResource({
      functionsResponse: {
        gateway: {
          name: "public-api",
          namespace: "public",
        },
        functions: {},
        schema: specSchema,
        mapping,
      },
    });

    expect(actualResource.body.apiVersion).to.equal(expectedResource.body.apiVersion);
    expect(actualResource.body.kind).to.equal(expectedResource.body.kind);
    expect(actualResource.body.metadata.name).to.equal(expectedResource.body.metadata.name);
    expect(actualResource.body.metadata.namespace).to.equal(expectedResource.body.metadata.namepspace);
    expect(actualResource.group).to.equal(expectedResource.group);
    expect(actualResource.namespace).to.equal(expectedResource.namespace);
    expect(actualResource.plural).to.equal(expectedResource.plural);
    expect(actualResource.version).to.equal(expectedResource.version);
    const actualMapping = loadYaml(actualResource.body.spec.map) as Record<string, Map>;
    expect(actualMapping?.expected?.url).to.equal(mapping.expected.url);
    expect(actualMapping?.expected?.options?.method).to.equal(mapping.expected.options.method);
  });
});
